REVERSE LIST

rev [] = []
rev (x:xs) = (rev xs) ++ [x]

main = putStrLn (show rev [])
