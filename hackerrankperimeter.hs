--distance between 2 coordinate points
d :: [Float] -> Float
d [x1,y1,x2,y2] = sqrt $ (x2 - x1)^2 + (y2 - y1)^2
d _ = 0 --0 if not 4 part list

--takes cycle of f (x1,y1,x2,y2...), and list length
perimeter' :: [Float] -> Float -> Float
perimeter' [] acc = acc
perimeter' points acc = perimeter' (drop 2 points) (acc + (d (take 4 points)))

perimeter :: [Float] -> Float
perimeter points = perimeter' (points ++ (take 2 points)) 0

main :: IO ()
main = do
    _ <- getLine
    x <- getContents
    let f = map (read::String->Float) $ words x
    print (perimeter f)